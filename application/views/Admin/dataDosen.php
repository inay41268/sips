<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sistem Informasi
        <small>Pengajuan Skripsi</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
      <?php if ($this->session->flashdata('message_success')) : ?>
        <div class="text-success">
          <?php echo $this->session->flashdata('message_success') ?>
        </div>
      <?php endif ?>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <!-- Form modal tambah -->
        <form class="form-inline" action="<?= base_url('Admin/importDosen'); ?>" method="post" enctype="multipart/form-data">
          <div class="form-group"><input type="file" name="file"></div>
          <div class="form-group"><input class='btn btn-primary' type="submit" value="Import Dosen"></div>
          <a type="button" class="btn btn-danger" href="<?php echo base_url("Admin/exportMhs"); ?>">Export Dosen</a>
          <button type="button" class="btn btn-success" id="tombol-tambah" data-toggle="modal" data-target="#form" >Tambah Dosen</button>
        </form>
        <div class="modal fade" id="form">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Tambah Dosen</h4>
            </div>
            <form id="form-tambah">
                <div class="modal-body">
                <input type="hidden" name="id" id="id" class="form-control">
                <div class="form-group">
                    <label for="">Nama Dosen</label>
                    <input class="form-control" type="text" name="nama" id="nama">
                </div>
                <div class="form-group">
                    <label for="">NIP</label>
                    <input class="form-control" type="date" name="ttl" id="ttl">
                </div>
                <div class="form-group">
                    <label for="">Gender</label>
                    <input class="form-control" type="text" name="domisili" id="domisili">
                </div>
                <div class="form-group">
                    <label for="">Prodi</label>
                    <input class="form-control" type="text" name="domisili" id="domisili">
                </div>
                <div class="form-group">
                    <label for="">Pendidikan</label>
                    <input class="form-control" type="text" name="domisili" id="domisili">
                </div>
                <div class="form-group">
                    <label for="">Username</label>
                    <input class="form-control" type="text" name="domisili" id="domisili">
                </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Kirim</button>
                </div>
            </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->

        </div>

          <div class="box">
              <h3>Data Dosen</h3>
        <!-- Posts List -->
        <table class="table table-borderd table-striped table-hover" id='postsList' width="100%" min-width="100%" >
            <thead>
            <tr>
                <th>No</th>
                <th>ID Dosen</th>
                <th>Nama</th>
                <th>NIP</th>
                <th>Gender</th>
                <th>Program Studi</th>
                <th>Pendidikan</th>
            </tr>
            </thead>
            <tbody id="tbl_data">

            </tbody>
        </table>

      <!-- Paginate -->
                <div id='pagination'></div>
            </div>
        </div>
    </div>

          </div>
          <!-- /.box -->

    
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  

<!-- Script -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->


<script type='text/javascript'>
  var table;
  $(document).ready(function() {
    table = $('#postsList').DataTable({
      ajax: "<?= base_url("Admin/dataDosen") ?>",

      columns: [{
          data: 'no'
        },
        {
          data: 'id_dosen'
        },
        {
          data: 'nama'
        },
        {
          data: 'nip'
        },
        {
          data: 'gender'
        },
        {
          data: 'prodi'
        },
        {
          data: 'pendidikan'
        },
      ],
    });

    $('#form').on('show.bs.modal', function(event) {
      var button = event.relatedTarget;
      var nama = button.getAttribute('data-nama');
      var ttl = button.getAttribute('data-ttl');
      var domisili = button.getAttribute('data-domisili');
      var id = button.getAttribute('data-id');
      $('#nama').val(nama);
      $('#ttl').val(ttl);
      $('#domisili').val(domisili);
      $('#id').val(id);
    });

    $('#form-edit').on('submit', function(event) {
      event.preventDefault();
      var form = $(this);
      $.ajax({
        url: "<?= base_url('item/edit') ?>",
        data: form.serialize(),
        type: 'POST',
        dataType: 'JSON',
        success: function(data) {
          $('#form').modal('hide');
          table.ajax.reload();
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            showCloseButton: true,
            timer: 5000,
            timeProgressBar: true,
            didOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
          })
          Toast.fire({
            icon: 'success',
            title: data.message
          });
        }
      });
    });

    $('#form-tambah').submit(function(event) {
      event.preventDefault();
      var form = $(this);
      $('.invalid-feedback').html('');     
      $.ajax({
        url: "<?= base_url('item/tambah') ?>",
        type: 'POST',
        dataType: 'JSON',
        data: form.serialize(),
        success: function(data) {
          if (data.success) {
            $('#form-tambah')[0].reset();
            table.ajax.reload();
            const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              showCloseButton: true,
              timer: 5000,
              timeProgressBar: true,
              didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            });
            Toast.fire({
                icon: 'success',
                title: data.message
              });
          } else {
            $.each(data.message, function(key, val) {
              if (val != "") {
                $('.invalid-feedback[for="' + key + '"]').html(val);    
              }
            })
          }
        }
      });
    });
  });
</script>

<script>
  window.setTimeout(function(){
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
      $(this).remove();
    });
  }, 3000)
</script>