<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sistem Informasi
        <small>Pengajuan Skripsi</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <section class="content-header">
        <div class="row" style="margin: 0.2%;">
        <!-- Form modal tambah grup-->
        <button type="button" class="btn btn-primary" id="tombol-tambah" data-toggle="modal" data-target="#form" >
          <i class="fa fa-lg fa-fw fa-plus" aria-hidden="true"></i>Tambah Group
        </button>
        <div class="modal fade" id="form">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Form Tambah Group User</h4>
                </div>
                <form id="form-tambah">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Nama Group</label>
                            <input class="form-control" type="text" name="nama_group" id="nama_group">
                        </div>
                        <div class="form-group">
                            <label for="">Status</label>
                            <select class="form-control" name="status" id="status">
                              <option value="0">--pilih--</option>
                              <option value="">1</option>
                              <option value="">0</option> 
                            </select>                
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
                </div>
                <!-- /.modal-content -->
            </div>
        </div>

        <!-- Form modal akses menu-->
        <div class="modal fade" id="form-akses">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Menu Akses Management</h4>
              </div>
              <form id="formAkses">
                    <div class="modal-body">
                    <table class="table table-borderd table-striped table-hover" id='' width="100%" min-width="100%" >
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Mneu</th>
                            <th>Akses</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
          </div>
        </div>



            <div class="box">
                <h3>Menu Management</h3>
                <!-- Posts List -->
                <table class="table table-borderd table-striped table-hover" id='postsList' width="100%" min-width="100%" >
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama Group</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody id="tbl_data">

                    </tbody>
                </table>
            </div>
        </div>  
    </section>
</div>

<!-- Data Table -->
<script type='text/javascript'>
    $(document).ready(function() {
        table = $('#postsList').DataTable({
        ajax: "<?= base_url("Admin/menuManagement") ?>",
        columns: [{
            data: 'no'
            },
            {
            data: 'nama_group'
            },
            {
            data: 'action'
            },
        ],
        });
    });

    $('#form-tambah').submit(function(event) {
      event.preventDefault();
      var form = $(this);
      $('.invalid-feedback').html('');     
      $.ajax({
        url: "<?= base_url('Admin/tambahGroup') ?>",
        type: 'POST',
        dataType: 'JSON',
        data: form.serialize(),
        success: function(data) {
          if (data.success) {
            $('#form-tambah')[0].reset();
            table.ajax.reload();
            const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              showCloseButton: true,
              timer: 5000,
              timeProgressBar: true,
              didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            });
            Toast.fire({
                icon: 'success',
                title: data.message
              });
          } else {
            $.each(data.message, function(key, val) {
              if (val != "") {
                $('.invalid-feedback[for="' + key + '"]').html(val);    
              }
            })
          }
        }
      });
    });

    $('form-akses').submit(function(event){

    });
</script>