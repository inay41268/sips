<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sistem Informasi
        <small>Pengajuan Skripsi</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
      <?php if ($this->session->flashdata('message_success')) : ?>
        <div class="text-success">
          <?php echo $this->session->flashdata('message_success') ?>
        </div>
      <?php endif ?>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <!-- Form modal tambah -->
        <form class="form-inline" action="<?= base_url('Admin/importMhs'); ?>" method="post" enctype="multipart/form-data">
          <div class="form-group"><input type="file" name="file"></div>
          <div class="form-group"><input class='btn btn-primary' type="submit" value="Import Mahasiswa"></div>
          <a type="button" class="btn btn-danger" href="<?php echo base_url("Admin/exportMhs"); ?>">Export Mahasiswa</a>
          <button type="button" class="btn btn-success" id="tombol-tambah" data-toggle="modal" data-target="#form" >Tambah Mahasiswa</button>
        </form>
        <div class="modal fade" id="form">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Data Mahasiswa</h4>
            </div>
            <form id="form-tambah">
                <div class="modal-body">
                <input type="hidden" name="id" id="id" class="form-control">
                <div class="form-group">
                    <label for="">Nama Mahasiswa</label>
                    <input class="form-control" type="text" name="nama" id="nama">
                </div>
                <div class="form-group">
                    <label for="">NIM</label>
                    <input class="form-control" type="text" name="nim" id="nim">
                </div>
                <div class="form-group">
                    <label for="">NIK</label>
                    <input class="form-control" type="text" name="nik" id="nik">
                </div>
                <div class="form-group">
                    <label for="">Program Studi</label>
                    <select class="form-control" name="prodi" id="prodi" value="<?= set_value('prodi');?>">
                      <option value="0">--pilih--</option>
                      <?php foreach ($prodi as $p):?>
                          <option value="<?php echo $p->id?>"><?php echo $p->prodi?></option>
                      <?php endforeach;?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Agama</label>
                    <select class="form-control" name="agama" id="agama" value="<?= set_value('agama');?>">
                        <option value="0">--pilih--</option>
                        <?php foreach ($agama as $agm):?>
                            <option value="<?php echo $agm->id?>"><?php echo $agm->agama?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Tempat Lahir</label>
                    <input class="form-control" type="text" name="t_lahir" id="t_lahir">
                </div>
                <div class="form-group">
                    <label for="">Tanggal Lahir</label>
                    <input class="form-control" type="date" name="tgl_lahir" id="tgl_lahir">
                </div>
                <div class="form-group">
                  <label for="">HP/WA</label>
                  <input class="form-control" placeholder="Hp/Wa" name="hp">     
                </div>
                <div class="form-group">
                  <label for="">Email</label>
                  <input class="form-control" placeholder="Email" name="email">      
                </div>
                <div class="form-group">
                  <label for="">Provinsi</label>
                    <select class="form-control" name="prov_id" id="prov_id" value="<?= set_value('prov_id');?>">
                      <option value="0">--pilih--</option>
                        <?php foreach ($dataprov as $prov):?>
                          <option value="<?php echo $prov->prov_id;?>"><?php echo $prov->prov_name;?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="form-group">
                  <label for="">Kabupaten</label>
                    <select class="form-control" name="kab_id" id="kab_id" value="<?= set_value('kab_id');?>">
                      <option value="0">--pilih--</option>
                    </select>
                </div>
                <div class="form-group has-feedback">
                  <label for="">Kecamatan</label>
                  <select class="form-control" name="kecamatan" id="kecamatan" value="<?= set_value('kecamatan');?>">
                      <option value="0">--pilih--</option>
                  </select>      
                </div>
                <div class="form-group has-feedback">
                  <label for="">Alamat</label>
                  <textarea class="form-control" name="alamat" id="alamat" value="<?= set_value('alamat');?>"></textarea>      
                </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Kirim</button>
                </div>
            </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->

        </div>


          <div class="box">
              <h3>Data Mahasiswa</h3>
        <!-- Posts List -->

        <table class="table table-borderd table-striped table-hover" id='postsList' width="100%" min-width="100%" >
            <thead>
            <tr>
                <th>No</th>
                <th>ID Mahasiswa</th>
                <th>Nama</th>
                <th>NIM</th>
                <th>NIK</th>
                <th>Agama</th>
                <th>Program Studi</th>
                <th>Hp/Wa</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody id="tbl_data">
              
            </tbody>
        </table>

      <!-- Paginate -->
                <div id='pagination'></div>
            </div>
        </div>
    </div>

          </div>
          <!-- /.box -->

    
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  
<!-- Data Table -->
<script type='text/javascript'>
  var table;
  $(document).ready(function() {
    table = $('#postsList').DataTable({
      ajax: "<?= base_url("admin/dataMhs") ?>",

      columns: [{
          data: 'no'
        },
        {
          data: 'id_mhs'
        },
        {
          data: 'nama'
        },
        {
          data: 'nim'
        },
        {
          data: 'nik'
        },
        {
          data: 'agama'
        },
        {
          data: 'prodi'
        },
        {
          data: 'no_hp'
        },
        {
          data: 'status'
        },
      ],
    });

    $('#form-tambah').submit(function(event) {
      event.preventDefault();
      var form = $(this);
      $('.invalid-feedback').html('');     
      $.ajax({
        url: "<?= base_url('admin/tambahMhs') ?>",
        type: 'POST',
        dataType: 'JSON',
        data: form.serialize(),
        success: function(data) {
          if (data.success) {
            $('#form-tambah')[0].reset();
            table.ajax.reload();
            const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              showCloseButton: true,
              timer: 5000,
              timeProgressBar: true,
              didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            });
            Toast.fire({
                icon: 'success',
                title: data.message
              });
          } else {
            $.each(data.message, function(key, val) {
              if (val != "") {
                $('.invalid-feedback[for="' + key + '"]').html(val);    
              }
            })
          }
        }
      });
    });

  });
</script>

<!-- verifConfirm -->
<script>
    function verifConfirm(event){
      Swal.fire({
        title: 'Activate User',
        text: 'Are you sure to activate the user ?',
        icon: 'warning',
        showCloseButton: true,
        cancelButtonText: 'No',
        confirmButtonText: 'Yes Activate',
        confirmButtonColor: 'green'
      }).then(dialog=>{
        if (dialog.isConfirmed){
          $.ajax({
            url: event.dataset.deleteUrl,
            type: 'GET',
            dataType: "JSON",
            error: function(){
              alert('Something is wrong');
            },
            success: function(data){
              if(data.success){
                table.ajax.reload();
                const Toast = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  showCloseButton: true,
                  timer: 5000,
                  timeProgressBar: true,
                  diOpen: (toast) =>{
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                  }
                })
                Toast.fire({
                  icon: 'success',
                  title: data.message
                });
              }
            }
          })         
        }
      })
    }
</script>

<!-- Provinsi Kabupaten Kecamatan -->
<script type="text/javascript">
    $(document).ready(function(){
      loadkabupaten();
      loadkecamatan();
    });
    
    function loadkabupaten(){
      $("#prov_id").change(function(){
        var getprovinsi = $(this).val();

        $.ajax({
          type: "POST",
          dataType : "JSON",
          url : "<?= base_url();?>auth/getdatakab",
          data : {provinsi : getprovinsi},
          async : false,
          success : function(data){
            console.log(data);
            var html = '';
            var i;
            for (i=0; i<data.length;i++){
              html += '<option value="'+data[i].city_id+'">'+data[i].city_name+'</option>';
            }
            $("#kab_id").html(html);
          }
        });
      });
    }

    function loadkecamatan(){
      $("#kab_id").change(function(){
        var getkabupaten = $(this).val();
        console.log(getkabupaten);
        $.ajax({
          type: "POST",
          dataType : "JSON",
          url : "<?= base_url();?>auth/getdatakec",
          data : {kabupaten : getkabupaten},
          async : false,
          success : function(data){
            console.log(data);
            var html = '';
            var i;
            for (i=0; i<data.length;i++){
              html += '<option value="'+data[i].dis_id+'">'+data[i].dis_name+'</option>';
            }
            $("#kecamatan").html(html);
          }
        });
      });
    }   
  </script>
<script>
    window.setTimeout(function(){
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 3000)
</script>