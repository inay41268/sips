<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sistem Informasi
        <small>Pengajuan Skripsi</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <!-- Form modal tambah -->
        <button type="button" class="btn btn-primary" id="tombol-tambah" data-toggle="modal" data-target="#form" >
          <i class="fa fa-lg fa-fw fa-arrow-circle-up" aria-hidden="true"></i>Ajukan
        </button>
        <div class="modal fade" id="form">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Pengajuan Judul</h4>
            </div>
            <form id="form-tambah">
                <div class="modal-body">
                <div class="form-group">
                    <label for="">Nama Mahasiswa</label>
                    <select class="form-control" name="id_mhs" id="id_mhs" value="<?= set_value('id_mhs');?>">
                      <option value="0">--pilih--</option>
                      <?php foreach ($idMhs as $idm):?>
                          <option value="<?php echo $idm->id_mhs?>"><?php echo $idm->nama?></option>
                      <?php endforeach;?>
                    </select> 
                </div>
                <div class="form-group">
                    <label for="">NIM</label>
                    <input class="form-control" type="text" name="nim" id="nim">
                </div>
                <div class="form-group">
                    <label for="">Program Studi</label>
                    <select class="form-control" name="prodi" id="prodi" value="<?= set_value('prodi');?>">
                      <option value="0">--pilih--</option>
                      <?php foreach ($prodi as $p):?>
                          <option value="<?php echo $p->id?>"><?php echo $p->prodi?></option>
                      <?php endforeach;?>
                    </select>                
                </div>
                <div class="form-group">
                    <label for="">Angkatan</label>
                    <select class="form-control" name="angkatan" id="angkatan" value="<?= set_value('angkatan');?>">
                      <option value="0">--pilih--</option>
                      <?php foreach ($angkatan as $a):?>
                          <option value="<?php echo $a->id?>"><?php echo $a->angkatan?></option>
                      <?php endforeach;?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Dosen</label>
                    <select class="form-control" name="dosen" id="dosen" value="<?= set_value('dosen');?>">
                      <option value="0">--pilih--</option>
                      <?php foreach ($dosen as $d):?>
                          <option value="<?php echo $d->id_dosen?>"><?php echo $d->nama?></option>
                      <?php endforeach;?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Judul Skripsi</label>
                    <input class="form-control" type="text" name="judul" id="judul">
                </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Kirim</button>
                </div>
            </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->

        </div>

          <div class="box">
              <h3>Pengajuan Judul Skripsi</h3>
        <!-- Posts List -->
        <table class="table table-borderd table-striped table-hover" id='postsList' width="100%" min-width="100%" >
            <thead>
            <tr>
                <th>No</th>
                <th>ID Skripsi</th>
                <th>ID Mahasiswa</th>
                <th>Nama</th>
                <th>NIM</th>
                <th>Program Studi</th>
                <th>Angkatan</th>
                <th>Dosen</th>
                <th>Judul Skripsi</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody id="tbl_data">

            </tbody>
        </table>

      <!-- Paginate -->
                <div id='pagination'></div>
            </div>
        </div>
    </div>

          </div>
          <!-- /.box -->

    
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  

<!-- Script -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->


<script type='text/javascript'>
  var table;
  $(document).ready(function() {
    table = $('#postsList').DataTable({
      ajax: "<?= base_url("Mahasiswa/get_items") ?>",

      columns: [{
          data: 'no'
        },
        {
          data: 'id_skripsi'
        },
        {
          data: 'id_mhs'
        },
        {
          data: 'nama'
        },
        {
          data: 'nim'
        },
        {
          data: 'prodi'
        },
        {
          data: 'angkatan'
        },
        {
          data: 'dosen'
        },
        {
          data: 'judul'
        },
        {
          data: 'status_skripsi'
        },
      ],
    });

    $('#form').on('show.bs.modal', function(event) {
      var button = event.relatedTarget;
      var nama = button.getAttribute('data-nama');
      var ttl = button.getAttribute('data-ttl');
      var domisili = button.getAttribute('data-domisili');
      var id = button.getAttribute('data-id');
      $('#nama').val(nama);
      $('#ttl').val(ttl);
      $('#domisili').val(domisili);
      $('#id').val(id);
    });

    $('#form-tambah').submit(function(event) {
      event.preventDefault();
      var form = $(this);
      $('.invalid-feedback').html('');     
      $.ajax({
        url: "<?= base_url('mahasiswa/tambahJudul') ?>",
        type: 'POST',
        dataType: 'JSON',
        data: form.serialize(),
        success: function(data) {
          if (data.success) {
            $('#form-tambah')[0].reset();
            table.ajax.reload();
            const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              showCloseButton: true,
              timer: 5000,
              timeProgressBar: true,
              didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            });
            Toast.fire({
                icon: 'success',
                title: data.message
              });
          } else {
            $.each(data.message, function(key, val) {
              if (val != "") {
                $('.invalid-feedback[for="' + key + '"]').html(val);    
              }
            })
          }
        }
      });
    });
    
  });
</script>

<?php if ($this->session->flashdata('message_login_success')) : ?>
  <script>
    Swal.fire({
      icon: 'success',
      title: 'Yeee!',
      text: '<?= $this->session->flasdadta('message_login_success') ?>'
    })
  </script>
<?php endif ?>

<!-- </body>
</html> -->