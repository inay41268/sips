<div class="content-wrapper">
        <?php if ($this->session->flashdata('message_success')) : ?>
            <div class="invalid-feedback text-success">
            <?php echo $this->session->flashdata('message_success') ?>
            </div>
        <?php endif ?>
    <section class="content-header">
        <h1>
            Edit Profile
        </h1>  
    </section>

    <section class="content">
        <div class="box"> 
            <div class="row" style="margin: 5px;">
            <?= form_open_multipart('EditProfile/aksiEdit', array('method' => 'post')); ?>
                <div class="col-lg-2">
                    <div class="form-group">
                        <img src="<?= base_url('assets/img/profile/').$user['avatar'];?>" alt="" class="img-thumbnail">                            
                        <label for="avatar">Ubah photo profil</label>
                        <input type="file" id="avatar" name="avatar" value="<?= $user['avatar']?>">   
                    </div>            
                </div>
                <div class="col-lg-5">
                    <div class="form-group">
                        <label for="">ID Mahasiswa</label>
                        <input type="text" class="form-control" id="id_user" name="id_user" value="<?= $user['id_user']?>" readonly>
                    </div>
                    <div class="form-group">
                        <label for="">Nama Mahasiswa</label>
                        <input type="text" class="form-control" id="nama" name ="nama" value="<?= $user['nama']?>">
                    </div>
                    <div class="form-group">
                        <label for="">NIM</label>
                        <input type="text" class="form-control" id="nim" name="nim" value="<?= $user['nim']?>">
                    </div>
                    <div class="form-group">
                        <label for="">NIK</label>
                        <input type="text" class="form-control" id="nik" name="nik" value="<?= $user['nik']?>">
                    </div>
                    <div class="form-group">
                        <label for="">Tempat Lahir</label>
                        <input type="text" class="form-control" id="t_lahir" name="t_lahir" value="<?= $user['tempat_lahir']?>">
                    </div>
                    <div class="form-group">
                        <label for="">Tanggal Lahir</label>
                        <input type="text" class="form-control" id="tgl_lahir" name="tgl_lahir" value="<?= $user['tgl_lahir']?>">
                    </div>
                    <div class="form-group">
                        <label for="">Agama</label>
                        <input type="text" class="form-control" id="agama" name="agama" value="<?= $user['agama']?>">
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="form-group">
                        <label for="">Prodi</label>
                        <input type="text" class="form-control" id="prodi" name="prodi" value="<?= $user['prodi']?>">
                    </div>
                    <div class="form-group">
                        <label for="">Hp/Wa</label>
                        <input type="text" class="form-control" id="hp" name="hp" value="<?= $user['no_hp']?>">
                    </div>
                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="text" class="form-control" id="email" name="email" value="<?= $user['email']?>">
                    </div>
                    <div class="form-group">
                        <label for="">Provinsi</label>
                        <input type="text" class="form-control" id="prov_id" name="prov_id" value="<?= $user['provinsi']?>">
                    </div>
                    <div class="form-group">
                        <label for="">Kabupaten</label>
                        <input type="text" class="form-control" id="kab_id" name="kab_id" value="<?= $user['kab_kota']?>">
                    </div>
                    <div class="form-group">
                        <label for="">Kecamatan</label>
                        <input type="text" class="form-control" id="kecamatan" name="kecamatan" value="<?= $user['kec']?>">
                    </div>
                    <div class="form-group">
                        <label for="">Alamat</label>
                        <input type="text" class="form-control" id="alamat" name="alamat" value="<?= $user['alamat']?>">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary center-block">Simpan Perubahan</button>
            </div>
        </div>
    </section>
</div>
<script>
    window.setTimeout(function(){
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 3000)
</script>