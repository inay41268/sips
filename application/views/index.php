<!-- <!DOCTYPE html>
<html lang="en">
  <head>
    <title>Codeigniter 3 Ajax Pagination using Jquery Example - ItSolutionStuff.com</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <style type="text/css">
      html, body { font-family: 'Raleway', sans-serif; }
      a{ color: #007bff; font-weight: bold;}
    </style>
  </head> 
  <body> -->


  <div class="content-wrapper">
  <div class="card">
    <div class="card-header ">
    </div>
    <div class="card-body">
      <!-- Tambah Data -->
          <!-- <form class="form-inline" id="form-tambah">
            <div class="form-group">
            <div class="col-xs-12">
              <input type="text" class="form-control" placeholder="Nama" name="nama" >
            </div>
              <div class="invalid-feedback" for="nama"></div>

              <input type="date" class="form-control" placeholder="TTL" name="ttl" >
              <div class="invalid-feedback" for="ttl"></div>

              <input type="text" class="form-control" placeholder="Domisili" name="domisili" >
              <div class="invalid-feedback" for="domisili"></div>

            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-default" id="tombol-tambah">Tambah</button>
            </div>
          </form>
            <hr><hr><hr>     -->
         

      <!-- Posts List -->
      <table class="table table-borderd table-striped table-hover" id='postsList'>
        <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>TTL</th>
            <th>Domisili</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody id="tbl_data">

        </tbody>
      </table>

      <!-- Paginate -->
      <div id='pagination'></div>
    </div>
  </div>
</div>


<div class="modal fade" id="form">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Form Edit</h4>
      </div>
      <form id="form-edit">
        <div class="modal-body">
          <input type="hidden" name="id" id="id" class="form-control">
          <div class="form-group">
            <label for="">Nama</label>
            <input class="form-control" type="text" name="nama" id="nama">
          </div>
          <div class="form-group">
            <label for="">TTL</label>
            <input class="form-control" type="date" name="ttl" id="ttl">
          </div>
          <div class="form-group">
            <label for="">Domisili</label>
            <input class="form-control" type="text" name="domisili" id="domisili">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Script -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->


<script type='text/javascript'>
  var table;
  $(document).ready(function() {
    table = $('#postsList').DataTable({
      ajax: "<?= base_url("item/get_items") ?>",

      columns: [{
          data: 'no'
        },
        {
          data: 'nama'
        },
        {
          data: 'ttl'
        },
        {
          data: 'domisili'
        },
        {
          data: 'aksi'
        }
      ],
    });

    $('#form').on('show.bs.modal', function(event) {
      var button = event.relatedTarget;
      var nama = button.getAttribute('data-nama');
      var ttl = button.getAttribute('data-ttl');
      var domisili = button.getAttribute('data-domisili');
      var id = button.getAttribute('data-id');
      $('#nama').val(nama);
      $('#ttl').val(ttl);
      $('#domisili').val(domisili);
      $('#id').val(id);
    });

    $('#form-edit').on('submit', function(event) {
      event.preventDefault();
      var form = $(this);
      $.ajax({
        url: "<?= base_url('item/edit') ?>",
        data: form.serialize(),
        type: 'POST',
        dataType: 'JSON',
        success: function(data) {
          $('#form').modal('hide');
          table.ajax.reload();
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            showCloseButton: true,
            timer: 5000,
            timeProgressBar: true,
            didOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
          })
          Toast.fire({
            icon: 'success',
            title: data.message
          });
        }
      });
    });

    $('#form-tambah').submit(function(event) {
      event.preventDefault();
      var form = $(this);
      $('.invalid-feedback').html('');     
      $.ajax({
        url: "<?= base_url('item/tambah') ?>",
        type: 'POST',
        dataType: 'JSON',
        data: form.serialize(),
        success: function(data) {
          if (data.success) {
            $('#form-tambah')[0].reset();
            table.ajax.reload();
            const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              showCloseButton: true,
              timer: 5000,
              timeProgressBar: true,
              didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            });
            Toast.fire({
                icon: 'success',
                title: data.message
              });
          } else {
            $.each(data.message, function(key, val) {
              if (val != "") {
                $('.invalid-feedback[for="' + key + '"]').html(val);    
              }
            })
          }
        }
      });
    });
  });
</script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
  function deleteConfirm(event) {
    Swal.fire({
      title: 'Konfirmasi Hapus Data!',
      text: 'Yakin ingin menghapus data ini?',
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Tidak',
      confirmButtonText: 'Ya, Hapus',
      confirmButtonColor: 'red'
    }).then(dialog => {
      if (dialog.isConfirmed) {
        $.ajax({
          url: event.dataset.deleteUrl,
          type: 'GET',
          dataType: "JSON",
          error: function() {
            alert('Terjadi Kesalahan');
          },
          success: function(data) {
            if (data.success) {
              table.ajax.reload();
              const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                showCloseButton: true,
                timer: 5000,
                timeProgressBar: true,
                didOpen: (toast) => {
                  toast.addEventListener('mouseenter', Swal.stopTimer)
                  toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
              })
              Toast.fire({
                icon: 'success',
                title: data.message
              });
            } else alert('Something is GJ')
          }
        })
      }
    })
  }
</script>
<?php if ($this->session->flashdata('message_login_success')) : ?>
  <script>
    Swal.fire({
      icon: 'success',
      title: 'Yeee!',
      text: '<?= $this->session->flasdadta('message_login_success') ?>'
    })
  </script>
<?php endif ?>

<!-- </body>
</html> -->