<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sistem Informasi
        <small>Pengajuan Skripsi</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        </div>
          <div class="box">
              <h3>Accepting Judul Skripsi</h3>
        <!-- Posts List -->
        <table class="table table-borderd table-striped table-hover" id='postsList' width="100%" min-width="100%" >
            <thead>
            <tr>
                <th>No</th>
                <th>ID Skripsi</th>
                <th>Nama</th>
                <th>NIM</th>
                <th>Program Studi</th>
                <th>Angkatan</th>
                <th>Dosen</th>
                <th>Judul Skripsi</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody id="tbl_data">

            </tbody>
        </table>

      <!-- Paginate -->
                <div id='pagination'></div>
            </div>
        </div>
    </div>

          </div>
          <!-- /.box -->

    
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  

<!-- Script -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->


<script type='text/javascript'>
  var table;
  $(document).ready(function() {
    table = $('#postsList').DataTable({
      ajax: "<?= base_url("Dosen/get_items") ?>",

      columns: [{
          data: 'no'
        },
        {
          data: 'id_skripsi'
        },
        {
          data: 'id_mhs'
        },
        {
          data: 'nim'
        },
        {
          data: 'prodi'
        },
        {
          data: 'angkatan'
        },
        {
          data: 'dosen'
        },
        {
          data: 'judul'
        },
        {
          data: 'status_skripsi'
        },
      ],
    });


    
  });
</script>

<script>
    function verifConfirm(event){
      Swal.fire({
        title: 'Accepted Skripsi',
        text: 'Are you sure to accept skripsi ?',
        icon: 'warning',
        showCloseButton: true,
        cancelButtonText: 'No',
        confirmButtonText: 'Yes Accept',
        confirmButtonColor: 'green'
      }).then(dialog=>{
        if (dialog.isConfirmed){
          $.ajax({
            url: event.dataset.deleteUrl,
            type: 'GET',
            dataType: "JSON",
            error: function(){
              alert('Something is wrong');
            },
            success: function(data){
              if(data.success){
                table.ajax.reload();
                const Toast = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  showCloseButton: true,
                  timer: 5000,
                  timeProgressBar: true,
                  diOpen: (toast) =>{
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                  }
                })
                Toast.fire({
                  icon: 'success',
                  title: data.message
                });
              }
            }
          })         
        }
      })
    }
</script>