<div class="content-wrapper">
        <?php if ($this->session->flashdata('message_success')) : ?>
            <div class="invalid-feedback text-success">
            <?php echo $this->session->flashdata('message_success') ?>
            </div>
        <?php endif ?>
    <section class="content-header">
        <h1>
            Edit Profile
        </h1>  
    </section>

    <section class="content">
        <div class="box"> 
            <div class="row"   style="margin: 5px;">
            <?= form_open_multipart('EditProfile/aksiEditDosen', array('method' => 'post')); ?>
                <div class="col-lg-5">
                    <div class="form-group">
                        <img src="<?= base_url('assets/img/profile/').$user['avatar'];?>" alt="" class="img-thumbnail">                            
                        <label for="avatar">Ubah photo profil</label>
                        <input type="file" id="avatar" name="avatar">   
                    </div>            
                </div>
                <div class="col-lg-7">
                    <div class="form-group">
                        <label for="">ID Dosen</label>
                        <input type="text" class="form-control" id="id_dosen" name="id_dosen" value="<?= $user['id_dosen']?>" readonly>
                    </div>
                    <div class="form-group">
                        <label for="">Nama Dosen</label>
                        <input type="text" class="form-control" id="nama" name ="nama" value="<?= $user['nama']?>">
                    </div>
                    <div class="form-group">
                        <label for="">Username</label>
                        <input type="text" class="form-control" id="username" name="username" value="<?= $user['username']?>">
                    </div>
                    <div class="form-group">
                        <label for="">NIP</label>
                        <input type="text" class="form-control" id="nip" name="nip" value="<?= $user['nip']?>">
                    </div>
                    <div class="form-group">
                        <label for="">Program Studi</label>
                        <input type="text" class="form-control" id="prodi" name="prodi" value="<?= $user['prodi']?>">
                    </div>
                    <div class="form-group">
                        <label for="">Pendidikan</label>
                        <input type="text" class="form-control" id="pendidikan" name="pendidikan" value="<?= $user['pendidikan']?>">
                    </div>
                </div>            
                <button type="submit" class="btn btn-primary center-block">Simpan Perubahan</button>
            </div>
        </div>
    </section>
</div>
<script>
    window.setTimeout(function(){
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 3000)
</script>
