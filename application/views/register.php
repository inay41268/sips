<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Register</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="../../index2.html"><b>Halaman Register</b></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
      <p class="login-box-msg">Sign up to start your session</p>


      <form action="" method="POST">

        <div class="form-group has-feedback">
          <label for="">Nama Lengkap</label>
          <input type="text" class="form-control" placeholder="Nama Lengkap" name="nama" value="<?= set_value('nama'); ?>">
          <?= form_error('nama', '<div class="text-danger">', '</div>'); ?>
        </div>

        <div class="form-group has-feedback">
          <label for="">NIM</label>
          <input type="text" class="form-control" placeholder="NIM" name="nim" maxlength="10" value="<?= set_value('nim'); ?>">
          <?= form_error('nim', '<div class="text-danger">', '</div>'); ?>
        </div>

        <div class="form-group has-feedback">
          <label for="">NIK</label>
          <input type="text" class="form-control" placeholder="NIK" name="nik" maxlength="16" value="<?= set_value('nik'); ?>">
          <?= form_error('nik', '<div class="text-danger">', '</div>'); ?>
        </div>

        <div class="form-group has-feedback">
          <label for="">Program Studi</label>
          <select class="form-control" name="prodi" id="prodi" value="<?= set_value('prodi'); ?>">
            <option value="0">--pilih--</option>
            <?php foreach ($prodi as $p) : ?>
              <option value="<?php echo $p->id ?>"><?php echo $p->prodi ?></option>
            <?php endforeach; ?>
          </select>
          <?= form_error('prodi', '<div class="text-danger">', '</div>'); ?>
        </div>

        <div class="form-group has-feedback">
          <label for="">Agama</label>
          <select class="form-control" name="agama" id="agama" value="<?= set_value('agama'); ?>">
            <option value="0">--pilih--</option>
            <?php foreach ($agama as $agm) : ?>
              <option value="<?php echo $agm->id ?>"><?php echo $agm->agama ?></option>
            <?php endforeach; ?>
          </select>
          <?= form_error('agama', '<div class="text-danger">', '</div>'); ?>
        </div>

        <div class="form-group has-feedback">
          <label for="">Tempat Lahir</label>
          <input class="form-control" placeholder="Tempat Lahir" name="t_lahir" value="<?= set_value('t_lahir'); ?>">
          <?= form_error('t_lahir', '<div class="text-danger">', '</div>'); ?>
        </div>

        <div class="form-group has-feedback">
          <label for="">Tanggal Lahir</label>
          <input class="form-control" type="date" placeholder="Tanggal Lahir" name="tgl_lahir" value="<?= set_value('tgl_lahir'); ?>">
          <?= form_error('tgl_lahir', '<div class="text-danger">', '</div>'); ?>
        </div>

        <div class="form-group has-feedback">
          <label for="">HP/WA</label>
          <input class="form-control" placeholder="Hp/Wa" name="hp" value="<?= set_value('hp'); ?>">
          <?= form_error('hp', '<div class="text-danger">', '</div>'); ?>
        </div>

        <div class="form-group has-feedback">
          <label for="">Email</label>
          <input class="form-control" placeholder="Email" name="email" value="<?= set_value('email'); ?>">
          <?= form_error('email', '<div class="text-danger">', '</div>'); ?>
        </div>

        <div class="form-group has-feedback">
          <label for="">Provinsi</label>
          <select class="form-control" name="prov_id" id="prov_id" value="<?= set_value('prov_id'); ?>">
            <option value="0">--pilih--</option>
            <?php foreach ($dataprov as $prov) : ?>
              <option value="<?php echo $prov->prov_id; ?>"><?php echo $prov->prov_name; ?></option>
            <?php endforeach; ?>
          </select>
          <?= form_error('prov_id', '<div class="text-danger">', '</div>'); ?>
        </div>

        <div class="form-group has-feedback">
          <label for="">Kabupaten</label>
          <select class="form-control" name="kab_id" id="kab_id" value="<?= set_value('kab_id'); ?>">
            <option value="0">--pilih--</option>
          </select>
          <?= form_error('kab_id', '<div class="text-danger">', '</div>'); ?>
        </div>

        <div class="form-group has-feedback">
          <label for="">Kecamatan</label>
          <select class="form-control" name="kecamatan" id="kecamatan" value="<?= set_value('kecamatan'); ?>">
            <option value="0">--pilih--</option>
          </select>
          <?= form_error('kecamatan', '<div class="text-danger">', '</div>'); ?>
        </div>

        <div class="form-group has-feedback">
          <label for="">Alamat</label>
          <textarea class="form-control" name="alamat" id="alamat" value="<?= set_value('alamat'); ?>"></textarea>
          <?= form_error('alamat', '<div class="text-danger">', '</div>'); ?>
        </div>

        <div class="form-group has-feedback">
          <label for="">Password</label>
          <div class="input-group">
            <input class="form-control" type="password" id="pass1" placeholder="password" name="password" minlength="8" value="<?= set_value('password'); ?>">
            <span class="input-group-btn">
              <button type="button" class="btn" id="mybutton1" onclick="change1()"><i class="fa fa-eye-slash fa-lg"></i>
              </button>
            </span>
          </div>
          <?= form_error('password', '<div class="text-danger">', '</div>'); ?>
        </div>

        <div class="form-group has-feedback">
          <label for="">Konfirmasi Password</label>
          <div class="input-group">
            <input class="form-control" type="password" placeholder="konfirmasiPassword" name="confirm-password" id="pass2" minlength="8" value="<?= set_value('confirm-password'); ?>">
            <span class="input-group-btn">
              <button type="button" class="btn" id="mybutton2" onclick="change2()"><i class="fa fa-eye-slash fa-lg"></i>
              </button>
            </span>
          </div>
          <?= form_error('confirm-password', '<div class="text-danger">', '</div>'); ?>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-xs-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign Up</button>
            </div>

          </div>

          <!-- /.col -->
        </div>
      </form>

      <span href="register.html" class="text-center">Already have an account?<a href="<?= base_url('auth/login') ?>">Login !</a></span>

    </div>
    <!-- /.login-box-body -->
  </div>
  <!-- /.login-box -->

  <!-- jQuery 3 -->
  <script src="<?= base_url() ?>assets/js/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
  <!-- iCheck -->
  <script src="<?= base_url() ?>assets/js/icheck.min.js"></script>
  <script src="<?= base_url() ?>assets/js/icheck.min.js"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      loadkabupaten();
      loadkecamatan();
    });

    function loadkabupaten() {
      $("#prov_id").change(function() {
        var getprovinsi = $(this).val();

        $.ajax({
          type: "POST",
          dataType: "JSON",
          url: "<?= base_url(); ?>auth/getdatakab",
          data: {
            provinsi: getprovinsi
          },
          async: false,
          success: function(data) {
            console.log(data);
            var html = '';
            var i;
            for (i = 0; i < data.length; i++) {
              html += '<option value="' + data[i].city_id + '">' + data[i].city_name + '</option>';
            }
            $("#kab_id").html(html);
          }
        });
      });
    }

    function loadkecamatan() {
      $("#kab_id").change(function() {
        var getkabupaten = $(this).val();
        console.log(getkabupaten);
        $.ajax({
          type: "POST",
          dataType: "JSON",
          url: "<?= base_url(); ?>auth/getdatakec",
          data: {
            kabupaten: getkabupaten
          },
          async: false,
          success: function(data) {
            console.log(data);
            var html = '';
            var i;
            for (i = 0; i < data.length; i++) {
              html += '<option value="' + data[i].dis_id + '">' + data[i].dis_name + '</option>';
            }
            $("#kecamatan").html(html);
          }
        });
      });
    }
  </script>

  <!-- Fungsi ikon mata password dan konfirmasi password -->
  <script>
    function change1() {
      var x = document.getElementById('pass1').type;
      if (x == 'password') {
        document.getElementById('pass1').type = 'text';
        document.getElementById('mybutton1').innerHTML = '<i class="fa fa-eye-slash fa-lg"></i>'
      } else {
        document.getElementById('pass1').type = 'password';
        document.getElementById('mybutton1').innerHTML = '<i class="fa fa-eye fa-lg"></i>';
      }
    }

    function change2() {
      var x = document.getElementById('pass2').type;
      if (x == 'password') {
        document.getElementById('pass2').type = 'text';
        document.getElementById('mybutton2').innerHTML = '<i class="fa fa-eye-slash fa-lg"></i>'
      } else {
        document.getElementById('pass2').type = 'password';
        document.getElementById('mybutton2').innerHTML = '<i class="fa fa-eye fa-lg"></i>';
      }
    }
  </script>
</body>

</html>

<!-- REMAKE REMAKE REMAKE REMAKE REMAKE REMAKE REMAKE REMAKE REMAKE REMAKE-->

<!DOCTYPE html>

<html lang="en" class="light-style customizer-hide" dir="ltr" data-theme="theme-default" data-assets-path="../assets/" data-template="vertical-menu-template-free">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

  <title>Register Basic - Pages | Sneat - Bootstrap 5 HTML Admin Template - Pro</title>

  <meta name="description" content="" />

  <!-- Favicon -->
  <link rel="icon" type="image/x-icon" href="../assets/img/favicon/favicon.ico" />

  <!-- Fonts -->
  <link rel="preconnect" href="https://fonts.googleapis.com" />
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
  <link href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet" />

  <!-- Icons. Uncomment required icon fonts -->
  <link rel="stylesheet" href="../assets/vendor/fonts/boxicons.css" />

  <!-- Core CSS -->
  <link rel="stylesheet" href="../assets/vendor/css/core.css" class="template-customizer-core-css" />
  <link rel="stylesheet" href="../assets/vendor/css/theme-default.css" class="template-customizer-theme-css" />
  <link rel="stylesheet" href="../assets/css/demo.css" />

  <!-- Vendors CSS -->
  <link rel="stylesheet" href="../assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css" />

  <!-- Page CSS -->
  <!-- Page -->
  <link rel="stylesheet" href="../assets/vendor/css/pages/page-auth.css" />
  <!-- Helpers -->
  <script src="../assets/vendor/js/helpers.js"></script>

  <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
  <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
  <script src="../assets/js/config.js"></script>
</head>

<body>
  <!-- Content -->

  <div class="container-xxl">
    <div class="authentication-wrapper authentication-basic container-p-y">
      <div class="authentication-inner">
        <!-- Register Card -->
        <div class="card">
          <div class="card-body">
            <!-- Logo -->
            <div class="app-brand justify-content-center">
              <a href="index.html" class="app-brand-link gap-2">
                <span class="app-brand-logo demo">
                  <svg width="25" viewBox="0 0 25 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs>
                      <path d="M13.7918663,0.358365126 L3.39788168,7.44174259 C0.566865006,9.69408886 -0.379795268,12.4788597 0.557900856,15.7960551 C0.68998853,16.2305145 1.09562888,17.7872135 3.12357076,19.2293357 C3.8146334,19.7207684 5.32369333,20.3834223 7.65075054,21.2172976 L7.59773219,21.2525164 L2.63468769,24.5493413 C0.445452254,26.3002124 0.0884951797,28.5083815 1.56381646,31.1738486 C2.83770406,32.8170431 5.20850219,33.2640127 7.09180128,32.5391577 C8.347334,32.0559211 11.4559176,30.0011079 16.4175519,26.3747182 C18.0338572,24.4997857 18.6973423,22.4544883 18.4080071,20.2388261 C17.963753,17.5346866 16.1776345,15.5799961 13.0496516,14.3747546 L10.9194936,13.4715819 L18.6192054,7.984237 L13.7918663,0.358365126 Z" id="path-1"></path>
                      <path d="M5.47320593,6.00457225 C4.05321814,8.216144 4.36334763,10.0722806 6.40359441,11.5729822 C8.61520715,12.571656 10.0999176,13.2171421 10.8577257,13.5094407 L15.5088241,14.433041 L18.6192054,7.984237 C15.5364148,3.11535317 13.9273018,0.573395879 13.7918663,0.358365126 C13.5790555,0.511491653 10.8061687,2.3935607 5.47320593,6.00457225 Z" id="path-3"></path>
                      <path d="M7.50063644,21.2294429 L12.3234468,23.3159332 C14.1688022,24.7579751 14.397098,26.4880487 13.008334,28.506154 C11.6195701,30.5242593 10.3099883,31.790241 9.07958868,32.3040991 C5.78142938,33.4346997 4.13234973,34 4.13234973,34 C4.13234973,34 2.75489982,33.0538207 2.37032616e-14,31.1614621 C-0.55822714,27.8186216 -0.55822714,26.0572515 -4.05231404e-15,25.8773518 C0.83734071,25.6075023 2.77988457,22.8248993 3.3049379,22.52991 C3.65497346,22.3332504 5.05353963,21.8997614 7.50063644,21.2294429 Z" id="path-4"></path>
                      <path d="M20.6,7.13333333 L25.6,13.8 C26.2627417,14.6836556 26.0836556,15.9372583 25.2,16.6 C24.8538077,16.8596443 24.4327404,17 24,17 L14,17 C12.8954305,17 12,16.1045695 12,15 C12,14.5672596 12.1403557,14.1461923 12.4,13.8 L17.4,7.13333333 C18.0627417,6.24967773 19.3163444,6.07059163 20.2,6.73333333 C20.3516113,6.84704183 20.4862915,6.981722 20.6,7.13333333 Z" id="path-5"></path>
                    </defs>
                    <g id="g-app-brand" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                      <g id="Brand-Logo" transform="translate(-27.000000, -15.000000)">
                        <g id="Icon" transform="translate(27.000000, 15.000000)">
                          <g id="Mask" transform="translate(0.000000, 8.000000)">
                            <mask id="mask-2" fill="white">
                              <use xlink:href="#path-1"></use>
                            </mask>
                            <use fill="#696cff" xlink:href="#path-1"></use>
                            <g id="Path-3" mask="url(#mask-2)">
                              <use fill="#696cff" xlink:href="#path-3"></use>
                              <use fill-opacity="0.2" fill="#FFFFFF" xlink:href="#path-3"></use>
                            </g>
                            <g id="Path-4" mask="url(#mask-2)">
                              <use fill="#696cff" xlink:href="#path-4"></use>
                              <use fill-opacity="0.2" fill="#FFFFFF" xlink:href="#path-4"></use>
                            </g>
                          </g>
                          <g id="Triangle" transform="translate(19.000000, 11.000000) rotate(-300.000000) translate(-19.000000, -11.000000) ">
                            <use fill="#696cff" xlink:href="#path-5"></use>
                            <use fill-opacity="0.2" fill="#FFFFFF" xlink:href="#path-5"></use>
                          </g>
                        </g>
                      </g>
                    </g>
                  </svg>
                </span>
                <span class="app-brand-text demo text-body fw-bolder">Sneat</span>
              </a>
            </div>
            <!-- /Logo -->
            <h4 class="mb-2">Adventure starts here 🚀</h4>
            <p class="mb-4">Make your app management easy and fun!</p>

            <form id="formAuthentication" class="mb-3" action="index.html" method="POST">
              <div class="mb-3">
                <label for="username" class="form-label">Username</label>
                <input type="text" class="form-control" id="username" name="username" placeholder="Enter your username" autofocus />
              </div>
              <div class="mb-3">
                <label for="email" class="form-label">Email</label>
                <input type="text" class="form-control" id="email" name="email" placeholder="Enter your email" />
              </div>
              <div class="mb-3 form-password-toggle">
                <label class="form-label" for="password">Password</label>
                <div class="input-group input-group-merge">
                  <input type="password" id="password" class="form-control" name="password" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;" aria-describedby="password" />
                  <span class="input-group-text cursor-pointer"><i class="bx bx-hide"></i></span>
                </div>
              </div>

              <div class="mb-3">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" id="terms-conditions" name="terms" />
                  <label class="form-check-label" for="terms-conditions">
                    I agree to
                    <a href="javascript:void(0);">privacy policy & terms</a>
                  </label>
                </div>
              </div>
              <button class="btn btn-primary d-grid w-100">Sign up</button>
            </form>

            <p class="text-center">
              <span>Already have an account?</span>
              <a href="auth-login-basic.html">
                <span>Sign in instead</span>
              </a>
            </p>
          </div>
        </div>
        <!-- Register Card -->
      </div>
    </div>
  </div>

  <!-- / Content -->

  <div class="buy-now">
    <a href="https://themeselection.com/products/sneat-bootstrap-html-admin-template/" target="_blank" class="btn btn-danger btn-buy-now">Upgrade to Pro</a>
  </div>

  <!-- Core JS -->
  <!-- build:js assets/vendor/js/core.js -->
  <script src="../assets/vendor/libs/jquery/jquery.js"></script>
  <script src="../assets/vendor/libs/popper/popper.js"></script>
  <script src="../assets/vendor/js/bootstrap.js"></script>
  <script src="../assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

  <script src="../assets/vendor/js/menu.js"></script>
  <!-- endbuild -->

  <!-- Vendors JS -->

  <!-- Main JS -->
  <script src="../assets/js/main.js"></script>

  <!-- Page JS -->

  <!-- Place this tag in your head or just before your close body tag. -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
</body>

</html>