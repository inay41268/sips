<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
        <title>Surat Persetujuan Skripsi</title>
        <style>
            
        </style>
    </head>
    <body>
        <img src="<?=base_url('/assets/img/logo-uwg.png')?>" style="position: absolute; width: 130px; height:auto; padding-top: 17px;">
        <pre style="margin: 0px; text-align: center; width: 100%; font-size: 18px; font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;">
            <b>Yayasan Pembina Pendidikan Indonesia Widyagama Malang</b>
            <b>UNIVERSITAS WIDYAGAMA MALANG</b>
            Kampus : Jl. Borobudur 35 Malang 65128
            Telp. (0341) 492282, 491648 Website: http://www.widyagama.ac.id
        </pre>

        <hr class="line" style="border: 3px; border-style: inset; border-top: 1px solid #0000; margin-top: 0px;">
        <h6 class="judul" style="text-align: center; margin-bottom: 30px; margin-top: 20px;">SURAT PERSETUJUAN SKRIPSI</h6>

        <p style="margin-bottom: 0px;">Saya yang bertanda tangan di bawah ini :</p>
        <table class="table table-sm table-borderless" style="margin-left: 20px;">
            <tr>
                <td>Nama</td>
                <td>:</td>
                <td><?= $this->session->userdata('id_group');?></td>
            </tr>
            <tr>
                <td style="width: 30%;">NIP</td>
                <td style="width: 5%;">:</td>
                <td style="width: 65%;">NIP Dosen</td>
            </tr>
            <tr>
                <td style="width: 30%; vertical-align: top;">Program Studi</td>
                <td style="width: 5%; vertical-align: top;">:</td>
                <td style="width: 65%;">Prodi dosen</td>
            </tr>
        </table>

        <p style="margin-top: 40px; margin-bottom: 0px;">telah menyetujui skripsi Mahasiswa : </p>

        <table class="table table-sm table-borderless" style="margin-left: 20px; margin-bottom: 100px;">
            <tr>
                <td style="width: 30%;">Nama</td>
                <td style="width: 5%;">:</td>
                <td style="width: 65%;">Nama Mahasiswa</td>
            </tr>
            <tr>
                <td style="width: 30%;">NIM</td>
                <td style="width: 5%;">:</td>
                <td style="width: 65%;">NIM Mahasiswa</td>
            </tr>
            <tr>
                <td style="width: 30%; vertical-align: top;">Program Studi</td>
                <td style="width: 5%; vertical-align: top;">:</td>
                <td style="width: 65%;">Prodi Mahasiswa</td>
            </tr>
            <tr>
                <td style="width: 30%;">Judul Skripsi</td>
                <td style="width: 5%;">:</td>
                <td style="width: 65%;">Judul Skripsi</td>
            </tr>
        </table>

        <div style="width: 50%; text-align: center; float: right;">Malang, 20 Januari 2022</div><br><br>
        <div style="width: 50%; text-align: center; float: right;">Yang bertanda tangan,</div><br><br><br><br><br>
        <div style="width: 50%; text-align: center; float: right;">Nama Dosen</div>

   
    </body>
</html>