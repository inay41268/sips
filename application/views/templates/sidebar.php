<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?= base_url()?>assets/img/profile/user.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Inayati Sa'adah</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- sidebar menu: : style can be found in sidebar.less -->
                      <!-- QUERY MENU -->
      <?php
      $id_group = $this->session->userdata('id_group');
      $queryMenu = "SELECT user_menu.id, id_group, menu, icon
                    FROM user_menu JOIN user_access_menu
                    ON user_menu.id = user_access_menu.menu_id
                    WHERE user_access_menu.id = $id_group
                    ORDER BY user_access_menu.menu_id ASC
                    ";
      $menu = $this->db->query($queryMenu)->result_array();
      ?>

      <ul class="sidebar-menu" data-widget="tree">
        <li class="treeview">
          
            <!-- LOOPIING MENU -->
            <a href="#">
              <?php foreach ($menu as $m): ?>
                <i class="<?= $m['icon']?>"></i> 
                <span><?= $m['menu']?></span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>            
            </a>
            <!-- LOOPING SUBMENU -->
            <ul class="treeview-menu">
              <li>
              <?php
              $menuId = $m['id'];
                $querySubMenu = "SELECT user_menu.id, user_sub_menu.icon, user_sub_menu.url, title
                                FROM user_sub_menu JOIN user_menu
                                ON user_sub_menu.menu_id = user_menu.id
                                WHERE user_sub_menu.menu_id = $menuId
                                ORDER BY menu_id ASC
                ";
              $subMenu = $this->db->query($querySubMenu)->result_array();
              // var_dump($subMenu);
              // die;
              ?>
                <?php foreach($subMenu as $sm):?>      
                    <a href="<?= base_url($sm['url'])?>">
                    <i class="<?= $sm['icon']?>" aria-hidden="true"></i>
                    <span><?= $sm['title']?></span></a>
                <?php endforeach;?>
                </li>
              </ul>

            <?php endforeach; ?>
          
        <!-- <li class="treeview">
          <a href="#">
          <i class="fa fa-database" aria-hidden="true"></i> <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= base_url('admin/v_mhs')?>"><i class="fa fa-users" aria-hidden="true"></i>Data Mahasiswa</a></li>
            <li><a href="<?= base_url('admin/v_dosen')?>"><i class="fa fa-users" aria-hidden="true"></i>Data Dosen</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
          <i class="fa fa-user-circle" aria-hidden="true"></i> <span>Menu Dosen</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= base_url('dosen/index')?>"><i class="fa fa-check-square-o" aria-hidden="true"></i>ACC Skripsi</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="login/forgot_password">
          <i class="fa fa-users" aria-hidden="true"></i> <span>Menu Mahasiswa</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?= base_url('mahasiswa/index')?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Kirim Judul</a></li>
            <li><a href="#"><i class="fa fa-spinner" aria-hidden="true"></i>Progress</a></li>
          </ul>
        </li> -->
        <li>
          <a href="<?php echo base_url('auth/forgot_password');?>">
          <i class="fa fa-unlock" aria-hidden="true"></i>
          <span>Change Password</span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url('auth/logout');?>">
            <i class="fa fa-sign-out"></i>
            <span>Log Out</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>