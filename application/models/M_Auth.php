<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Auth extends CI_Model {
    private $_table = "user";
    const SESSION_KEY = 'user_id';

    // public function set_user($user)
    // {
    //     return $this->db->insert('user_group', $user);
    // }

    public function provinsi(){
        $query = $this->db->query("SELECT * FROM provinces ORDER BY prov_name ASC");
        return $query->result();
    }
    public function kabupaten($prov_id){
        $query = $this->db->query("SELECT * FROM cities WHERE prov_id = '$prov_id' ORDER BY city_name ASC");
        return $query->result();
    }
    public function kecamatan($kab_id){
        $query = $this->db->query("SELECT * FROM districts WHERE city_id = '$kab_id' ORDER BY dis_name ASC");
        return $query->result();
    }
    public function agama(){
        $query = $this->db->query("SELECT * FROM agama");
        return $query->result();
    }
    public function prodi(){
        $query = $this->db->query("SELECT * FROM prodi");
        return $query->result();
    }
    public function angkatan(){
        $query = $this->db->query("SELECT * FROM angkatan ORDER BY angkatan ASC");
        return $query->result();
    }
    public function dosen(){
        $query = $this->db->query("SELECT * FROM dosen");
        return $query->result();
    }
    public function idMhs(){
        $query = $this->db->query("SELECT * FROM mhs");
        return $query->result();
    }

    public function userMhs($username){
        $this->db->select('*');
		$this->db->from('user');
		// $this->db->join('mhs', 'user.id_user = mhs.id_user');
        $this->db->where('user.username', $username);
		$query = $this->db->get();
        return $query; 
    }

    public function login()
    {
        $query = $this->db->get('mhs');
        $user = $query->row_array();
        return $user;
    }

    public function change_password($email)
	{
        $query =  $this->db->get_where($this->_table, ['email' => $email]);
		$data = $query->row();
		return $data->email;	
    }

    public function update_password($email, $password){
        $this->db->update($this->_table, ['password'=>$password],['email' =>$email]);
        return $this->db->affected_rows();
    }

    public function current_user()
    {
        if ($this->session->has_userdata(self::SESSION_KEY))
        {
            return null;
        }
        $user_id = $this->session->userdata(self::SESSION_KEY);
        $query = $this->db->get_where($this->_table, ['id_user' => $user_id]);
        return $query->row();
    }

}
