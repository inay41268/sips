<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Dosen extends CI_Model {
    protected $table = 'mhs';
    protected $table2 = 'judul';

    public function getAll()
    {
        $this->db->select('*');
		$this->db->from('judul');
		$this->db->join('mhs', 'mhs.id_mhs = judul.id_mhs');
		$query = $this->db->get();
        return $query;
    }

    public function accSkripsi($id, $data){
        return $this->db->update('judul', $data,['id_mhs'=>$id]);
    }

    public function add_batch($data) {
		return $this->db->insert_batch('dosen', $data);
	}

    public function editProfile($data, $id)
    {         
        $this->db->select('*');
		$this->db->from('user');
		$this->db->join('dosen', 'dosen.id_user = user.id_user');
        $this->db->where('id_user', $data['id_user']);
        return $this->db->update('dosen', $data, ['id_user'=> $id]);
    }
}