<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Judul extends CI_Model {
    protected $table = 'judul';

    public function tambah($data){
        return $this->db->insert($this->table, $data);
    }

    public function getMhs($id_mhs){
        $this->db->select('s.id as id_skripsi, s.judul, s.status as status, mhs.nama as nama, dosen.nama as nama');
        $this->db->join('dosen', 'j.id_dosen = dosen.id');
        $this->db->where('mhs.id', $id_mhs);
        $query = $this->db->get();
        return $query;
    }
}