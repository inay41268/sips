<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_User extends CI_Model {

    protected $table = 'user';
    protected $table2 = 'mhs';

    public function set_user($user)
    {
        return $this->db->insert('user', $user);
    }
    public function activate($id, $data){
        return $this->db->update('mhs', $data,['id_mhs'=>$id]);
    }
    public function update_user($data, $id)
    {
        return $this->db->update($this->table, $data, ['id_user'=>$id]);
    }
    public function userGroup(){
        $query = $this->db->get('user_group');
        return $query->result();
    }
    public function addGroup($user_group){
        return $this->db->insert('user_group', $user_group);
    }

    // public function aksesGroup($id_group){
    //     $this->db->get_where('user_group',['id_group' => $id_group]);
    // }
    // public function aksesMenu(){
    //     $this->db->get('user_menu')->row_array();
    // }
}