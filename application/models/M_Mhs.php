<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Mhs extends CI_Model {

    protected $table = 'mhs';
    protected $table2 = 'judul';
    public function simpan($data)
    {
        return $this->db->insert('mhs', $data);
    }
    public function submitJudul($data)
    {
        return $this->db->insert('judul', $data);
    }

    public function dataMhs(){
        $this->db->select('*');
        $this->db->from('mhs');
        $query = $this->db->get();
        return $query;
    }

    public function exportMhs(){
        return $this->db->get('mhs')->result(); // Tampilkan semua data yang ada di tabel siswa
    }
    public function add_batch($data) {
		return $this->db->insert_batch('mhs', $data);
	}

    public function getAll()
    {
        $this->db->select('*');
		$this->db->from('judul');
		$this->db->join('mhs', 'mhs.id_mhs = judul.id_mhs');
		$query = $this->db->get();
        // $query = $this->db->get_where(['id_mhs' => $id_mhs]);
        return $query;  
    }

    public function editProfile($data, $id)
    {         
        $this->db->select('*');
		$this->db->from('user');
		$this->db->join('mhs', 'mhs.id_user = user.id_user');
        $this->db->where('id_user', $data['id_user']);
        return $this->db->update('mhs', $data, ['id_user'=> $id]);
    }
}