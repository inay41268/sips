<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->table = 'judul'; 
        
        $this->load->model('M_Auth', 'auth');
        $this->load->model('M_Mhs', 'mhs');
        $this->load->model('M_User', 'user');

        // if($this->auth->current_user()){
        //     redirect('auth/login');
        // }
        // $this->auth->login();
    }

    public function index(){
        $data_prodi = $this->auth->prodi();
        $data['prodi'] = $data_prodi;
        $data_angkatan = $this->auth->angkatan();
        $data['angkatan'] = $data_angkatan;
        $data_dosen = $this->auth->dosen();
        $data['dosen'] = $data_dosen;
        $data_idMhs= $this->auth->idMhs();
        $data['idMhs'] = $data_idMhs;

        $this->load->view('templates/header');
		$this->load->view('templates/sidebar');
        $this->load->view('mahasiswa/judul', $data);
		$this->load->view('templates/footer');  
    }

    public function get_items(){
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $query = $this->mhs->getAll();

        $data = [];
        // $id = $this->session->userdata('id_user');
        foreach($query->result() as $key => $r){
            $data[] = array(
                'no' => $key+1,
                'id_skripsi' => $r->id_skripsi,
                'id_mhs' =>$r->id_mhs,
                'nama' => $r->nama,
                'nim' =>$r->nim,
                'prodi' => $r->prodi,
                'angkatan' =>$r->angkatan,
                'dosen' =>$r->dosen,
                'judul' => $r->judul,
                'status_skripsi' => ($r->status_skripsi == "1" ? '<a href="'.site_url("PdfView/index"). '" data-delete-url="' . site_url("dosen/acc/" . $r->id_mhs) . '" role="button" onclick="return verifConfirm(this)" class="btn btn-success hapus">Cetak Bukti Acc</a>': '<h4><span class="label label-warning">Waiting</span></h4>
                ')
            ); 
        }
        $result = array(
            "draw" => $draw,
            "recordsTotal" => $query->num_rows(),
            "recordFiltered" => $query->num_rows(),
            "data" => $data
        );
        echo json_encode($result);
        exit();
    }

    public function tambahJudul(){
        $this->form_validation->set_rules('id_mhs', 'Id_mhs', 'required');
        $this->form_validation->set_rules('nim', 'Nim', 'required');
        $this->form_validation->set_rules('prodi', 'Prodi', 'required');
        $this->form_validation->set_rules('angkatan', 'Angkatan', 'required');
        $this->form_validation->set_rules('dosen', 'Dosen', 'required');
        $this->form_validation->set_rules('judul', 'Judul', 'required');
        
        $this->form_validation->set_message('required', '{field} harus diisi!');

                if($this->form_validation->run()=== FALSE){
                    $res = [];
                    foreach ($this->input->post() as $key => $value){
                        $res[$key] = form_error($key, '<div class="text-danger">','</div>');
                    }
                    echo json_encode(['success' => false, 'message' => $res]);
                }else{
                    $data = [
                        'id_mhs' => $this->input->post('id_mhs'),
                        'nim'=> $this->input->post('nim'),
                        'prodi' => $this->input->post('prodi'),
                        'angkatan' => $this->input->post('angkatan'),
                        'dosen' => $this->input->post('dosen'),
                        'judul' => $this->input->post('judul'),
                         
                    ];

                    $this->mhs->submitJudul($data);
                    echo json_encode(['success' =>true, 'message' => 'Data Berhasil Ditambahkan!']);
                }
    }

}

?>