<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PdfView extends CI_Controller {
    public function index()
    {
        // panggil library yang kita buat sebelumnya yang bernama pdf
        $this->load->library('pdf');
        
        // title dari pdf
        $this->data['title_pdf'] = 'Laporan Persetujuan Skripsi';
        
        // filename dari pdf ketika didownload
        $file_pdf = 'laporan_persetujuan_skripsi';
        // setting paper
        $paper = 'A4';
        //orientasi paper potrait / landscape
        $orientation = "portrait";
        
		$html = $this->load->view('Pdf/viewPdf',$this->data, true);	    
        
        // run dompdf
        $this->pdf->generate($html, $file_pdf,$paper,$orientation);
    }

    public function tes(){
        $this->load->view('Pdf/viewPdf');
    }
}