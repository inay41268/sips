<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EditProfile extends CI_Controller {
    var $user;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Auth', 'auth');
        $this->load->model('M_Mhs', 'mhs');
        $this->load->model('M_Dosen', 'dosen');
        $this->load->model('M_User', 'user');
    }

    public function editProfileMhs(){
        $data_prodi = $this->auth->prodi();
        $data['prodi'] = $data_prodi;
        $data_angkatan = $this->auth->angkatan();
        $data['angkatan'] = $data_angkatan;
        $data_dosen = $this->auth->dosen();
        $data['dosen'] = $data_dosen;
        $data_idMhs= $this->auth->idMhs();
        $data['idMhs'] = $data_idMhs;
        $data['user'] = $this->db->get_where('mhs', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('mahasiswa/profile', $data);
        $this->load->view('templates/footer');

    }

    public function aksiEdit($id=null)
    {
        $user = array(
            'username' => $this->input->post('email'),
            'email' => $this->input->post('email'),
            'id_group' => $this->session->userdata('id_group')
        );

        $id = $this->session->userdata('id_user');
        $this->user->update_user($user, $id);
        $data = [
                    'id_user' => $id,
                    'nim' => $this->input->post('nim'),
                    'nama' => $this->input->post('nama'),
                    'nik'=> $this->input->post('nik'),
                    'tempat_lahir' => $this->input->post('t_lahir'),
                    'tgl_lahir' => $this->input->post('tgl_lahir'),
                    'agama' => $this->input->post('agama'),
                    'no_hp' => $this->input->post('hp'),
                    'email' => $this->input->post('email'),
                    'provinsi' => $this->input->post('prov_id'),
                    'kab_kota' => $this->input->post('kab_id'),
                    'kec' => $this->input->post('kecamatan'),
                    'alamat' => $this->input->post('alamat'),
                    'prodi' => $this->input->post('prodi'),
                    // 'avatar' => $this->input->get('avatar'),
                ]; 

        // cek ada atau tidak gambar yang akan diupload
        $upload_image = $_FILES['avatar']['name'];
        
        if($upload_image){
            $config['upload_path'] = './assets/img/profile/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size']     = '2048';
            $config['max_width'] = '1024';
            $config['max_height'] = '768';

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('avatar')){
                $oldAvatar = $data['avatar'];
                if($oldAvatar != 'user.png'){
                    unlink(FCPATH . 'assets/img/profile/' . $oldAvatar);
                }

                $newAvatar = $this->upload->data('file_name');
                $this->db->set('avatar', $newAvatar);
            }else{
                echo $this->upload->display_errors();
            }
        }
        
        $edit = $this->mhs->editProfile($data, $id);
        if ($edit){
            $this->session->set_flashdata('message_success', '<div class="alert alert-success pull-right" style="width: 25%; height: 40px; margin: 5px 5px 5px 5px; padding: 10px;" role="alert">Data berhasil diupdate!</div>');
            return redirect('EditProfile/editProfileMhs');
        }    
    }

    public function editProfileDosen(){
        $data['user'] = $this->db->get_where('dosen', ['username' => $this->session->userdata('email')])->row_array();
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('Dosen/profile', $data);
        $this->load->view('templates/footer');
    }

    public function aksiEditDosen($id=null){
        $user = array(
            'username' => $this->input->post('username'),
            'email' => $this->input->post('username'),
            'id_group' => $this->session->userdata('id_group')
        );
        $id = $this->session->userdata('id_user');
        $this->user->update_user($user, $id);
        $data = [
                    'id_user' => $id,
                    'nip' => $this->input->post('nip'),
                    'nama' => $this->input->post('nama'),
                    'username'=> $this->input->post('username'),
                    'prodi' => $this->input->post('prodi'),
                    'pendidikan' => $this->input->post('pendidikan'),
                ]; 

        // cek ada atau tidak gambar yang akan diupload
        $upload_image = $_FILES['avatar']['name'];
        
        if($upload_image){
            $config['upload_path'] = './assets/img/profile/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size']     = '2048';
            $config['max_width'] = '1024';
            $config['max_height'] = '768';

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('avatar')){
                $oldAvatar = $data['avatar'];
                if($oldAvatar != 'user.png'){
                    unlink(FCPATH . 'assets/img/profile/' . $oldAvatar);
                }

                $newAvatar = $this->upload->data('file_name');
                $this->db->set('avatar', $newAvatar);
            }else{
                echo $this->upload->display_errors();
            }
        }
        
        $edit = $this->dosen->editProfile($data, $id);
        if ($edit){
            $this->session->set_flashdata('message_success', '<div class="alert alert-success pull-right" style="width: 25%; height: 40px; margin: 5px 5px 5px 5px; padding: 10px;" role="alert">Data berhasil diupdate!</div>');
            return redirect('EditProfile/editProfileDosen');
        }
    }

}