<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Include librari PhpSpreadsheet
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Admin extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->table = 'mhs'; 
        $this->table = 'dosen'; 
        $this->load->model('M_Mhs', 'mhs');
        $this->load->model('M_Dosen', 'dosen');
        $this->load->model('M_User', 'user');
        $this->load->model('M_Auth', 'auth');

        // if($this->session->userdata('username')==""){
        //     redirect('auth/login');
        // }
        // $this->load->model('M_Auth', 'auth');
    }

    public function index(){
        // $data['data_mhs'] = $this->mhs->dataMhs()->result();
        $this->load->view('templates/header');
		$this->load->view('templates/sidebar');
        $this->load->view('Admin/dashboard');
		$this->load->view('templates/footer');  
    }

    public function v_mhs(){
        $data_prodi = $this->auth->prodi();
        $data['prodi'] = $data_prodi;
        $data_angkatan = $this->auth->angkatan();
        $data['angkatan'] = $data_angkatan;
        $data_dosen = $this->auth->dosen();
        $data['dosen'] = $data_dosen;
        $data_idMhs= $this->auth->idMhs();
        $data['idMhs'] = $data_idMhs;
        $data_idMhs= $this->auth->idMhs();
        $data['idMhs'] = $data_idMhs;
        $data_agama= $this->auth->agama();
        $data['agama'] = $data_agama;
        $getdata = $this->auth->provinsi();
        $data['dataprov'] = $getdata;

        $this->load->view('templates/header');
		$this->load->view('templates/sidebar');
        $this->load->view('Admin/dataMhs', $data);
		$this->load->view('templates/footer');  
    }

    public function v_dosen(){
        $this->load->view('templates/header');
		$this->load->view('templates/sidebar');
        $this->load->view('Admin/dataDosen');
		$this->load->view('templates/footer');  
    }

    public function v_menu(){
        $data_status= $this->user->userGroup();
        $data['is_active'] = $data_status;
        // var_dump($data);
        // die;
        $this->load->view('templates/header');
		$this->load->view('templates/sidebar');
        $this->load->view('Admin/aksesMenu', $data);
		$this->load->view('templates/footer');
    }

    public function tambahMhs(){
        $user = array(
            'username' => $this->input->post('email'),
            'email' => $this->input->post('email'),
            'password' => hash('sha256', $this->input->post('nik')),
            'id_group' => 2
        );

        $this->user->set_user($user);
        $id_user = $this->db->insert_id();
        $data = [
                    'id_user' => $id_user,
                    'nim' => $this->input->post('nim'),
                    'nama' => $this->input->post('nama'),
                    'nik'=> $this->input->post('nik'),
                    'tempat_lahir' => $this->input->post('t_lahir'),
                    'tgl_lahir' => $this->input->post('tgl_lahir'),
                    'agama' => $this->input->post('agama'),
                    'no_hp' => $this->input->post('hp'),
                    'email' => $this->input->post('email'),
                    'provinsi' => $this->input->post('prov_id'),
                    'kab_kota' => $this->input->post('kab_id'),
                    'kec' => $this->input->post('kecamatan'),
                    'alamat' => $this->input->post('alamat'),
                    'prodi' => $this->input->post('prodi'),
                    // 'password' => hash('sha256', $this->input->post('password'))
                ]; 
        $this->mhs->simpan($data);
        echo json_encode(['success' =>true, 'message' => 'Data Berhasil Ditambahkan!']);

    }
    
    public function dataMhs(){
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $query = $this->mhs->dataMhs();

        $data = [];

        foreach($query->result() as $key => $r){
            $data[] = array(
                'no' => $key+1,
                'id_mhs' => $r->id_mhs,
                'nama' =>$r->nama,
                'nim' =>$r->nim,
                'nik' => $r->nik,
                'agama' =>$r->agama,
                'prodi' =>$r->prodi,
                'no_hp' =>$r->no_hp,
                'status' => ($r->status == "1" ? '<span class="badge badge-primary">Sudah aktif</span>' : '<a href="#" data-delete-url="' . site_url("admin/activateMhs/" . $r->id_mhs) . '" role="button" onclick="return verifConfirm(this)" class="btn btn-info hapus">Activate User</a>')
            ); 
        }
        $result = array(
            "draw" => $draw,
            "recordsTotal" => $query->num_rows(),
            "recordFiltered" => $query->num_rows(),
            "data" => $data
        );
        echo json_encode($result);
        exit();
    }

    public function activateMhs($id){
        $res = $this->user->activate($id, ['status' => 1]);
        if ($res)
            echo json_encode(['success' => true, 'message' => 'Akun Mahasiswa berhasil diaktivasi!']);
        else echo "gj";
    }

    public function dataDosen(){
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $query = $this->db->get("dosen");
        $data = [];
        foreach($query->result() as $key => $r){
            $data[] = array(
                'no' => $key+1,
                'id_dosen' => $r->id_dosen,
                'nama' =>$r->nama,
                'nip' =>$r->nip,
                'gender' => $r->gender,
                'prodi' =>$r->prodi,
                'pendidikan' =>$r->pendidikan
            ); 

        }
        $result = array(
            "draw" => $draw,
            "recordsTotal" => $query->num_rows(),
            "recordFiltered" => $query->num_rows(),
            "data" => $data
        );
        echo json_encode($result);
        exit();
    }

    public function exportMhs(){
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $style_col = [
            'font' => ['bold' => true], // Set font nya jadi bold
            'alignment' => [
              'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
              'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ],
            'borders' => [
              'top' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border top dengan garis tipis
              'right' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN],  // Set border right dengan garis tipis
              'bottom' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border bottom dengan garis tipis
              'left' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN] // Set border left dengan garis tipis
            ]
          ];

        // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
        $style_row = [
            'alignment' => [
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ],
            'borders' => [
            'top' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border top dengan garis tipis
            'right' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN],  // Set border right dengan garis tipis
            'bottom' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN], // Set border bottom dengan garis tipis
            'left' => ['borderStyle'  => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN] // Set border left dengan garis tipis
            ]
        ];

        $sheet->setCellValue('A1', "DATA MAHASISWA"); // Set kolom A1 dengan tulisan "DATA SISWA"
        $sheet->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai E1
        $sheet->getStyle('A1')->getFont()->setBold(true); // Set bold kolom A1

        // Buat header tabel nya pada baris ke 3
        $sheet->setCellValue('A3', "NO"); 
        $sheet->setCellValue('B3', "ID Mahasiswa");
        $sheet->setCellValue('C3', "Nama");
        $sheet->setCellValue('D3', "NIM");
        $sheet->setCellValue('E3', "NIK");
        $sheet->setCellValue('F3', "Agama");
        $sheet->setCellValue('G3', "Prodi");
        $sheet->setCellValue('H3', "Hp/Wa");
        $sheet->setCellValue('I3', "Email");

        // Apply style header yang telah kita buat tadi ke masing-masing kolom header
        $sheet->getStyle('A3')->applyFromArray($style_col);
        $sheet->getStyle('B3')->applyFromArray($style_col);
        $sheet->getStyle('C3')->applyFromArray($style_col);
        $sheet->getStyle('D3')->applyFromArray($style_col);
        $sheet->getStyle('E3')->applyFromArray($style_col);
        $sheet->getStyle('F3')->applyFromArray($style_col);
        $sheet->getStyle('G3')->applyFromArray($style_col);
        $sheet->getStyle('H3')->applyFromArray($style_col);
        $sheet->getStyle('I3')->applyFromArray($style_col);

        // Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
        $mahasiswa = $this->mhs->exportMhs();

        $no = 1; // Untuk penomoran tabel, di awal set dengan 1
        $numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
        foreach($mahasiswa as $data){ // Lakukan looping pada variabel siswa
            $sheet->setCellValue('A'.$numrow, $no);
            $sheet->setCellValue('B'.$numrow, $data->id_mhs);
            $sheet->setCellValue('C'.$numrow, $data->nama);
            $sheet->setCellValue('D'.$numrow, $data->nim);
            $sheet->setCellValue('E'.$numrow, $data->nik);
            $sheet->setCellValue('F'.$numrow, $data->agama);
            $sheet->setCellValue('G'.$numrow, $data->prodi);
            $sheet->setCellValue('H'.$numrow, $data->no_hp);
            $sheet->setCellValue('I'.$numrow, $data->email);
            
            // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
            $sheet->getStyle('A'.$numrow)->applyFromArray($style_row);
            $sheet->getStyle('B'.$numrow)->applyFromArray($style_row);
            $sheet->getStyle('C'.$numrow)->applyFromArray($style_row);
            $sheet->getStyle('D'.$numrow)->applyFromArray($style_row);
            $sheet->getStyle('E'.$numrow)->applyFromArray($style_row);
            $sheet->getStyle('F'.$numrow)->applyFromArray($style_row);
            $sheet->getStyle('G'.$numrow)->applyFromArray($style_row);
            $sheet->getStyle('H'.$numrow)->applyFromArray($style_row);
            $sheet->getStyle('I'.$numrow)->applyFromArray($style_row);
            
            $no++; // Tambah 1 setiap kali looping
            $numrow++; // Tambah 1 setiap kali looping
          }

        // Set width kolom
        $sheet->getColumnDimension('A')->setWidth(5); // Set width kolom A
        $sheet->getColumnDimension('B')->setWidth(15); // Set width kolom B
        $sheet->getColumnDimension('C')->setWidth(25); // Set width kolom C
        $sheet->getColumnDimension('D')->setWidth(20); // Set width kolom D
        $sheet->getColumnDimension('E')->setWidth(30); // Set width kolom E
        $sheet->getColumnDimension('F')->setWidth(15); // Set width kolom F
        $sheet->getColumnDimension('G')->setWidth(25); // Set width kolom G
        $sheet->getColumnDimension('H')->setWidth(20); // Set width kolom H
        $sheet->getColumnDimension('I')->setWidth(30); // Set width kolom I

        // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
        $sheet->getDefaultRowDimension()->setRowHeight(-1);
        // Set orientasi kertas jadi LANDSCAPE
        $sheet->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
        // Set judul file excel nya
        $sheet->setTitle("Laporan Data Mahasiswa");
        // Proses file excel
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="Data Mahasiswa.xlsx"'); // Set nama file excel nya
        header('Cache-Control: max-age=0');
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
    }

    public function importMhs(){
        $upload_file = $_FILES['file']['name'];
        $extension = pathinfo($upload_file, PATHINFO_EXTENSION);
        if($extension == 'csv'){
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
        }else if($extension =  'xlsx'){
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }else{
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        }
        $spreadsheet = $reader->load($_FILES['file']['tmp_name']);
        $sheetdata = $spreadsheet->getActiveSheet()->toArray();
        $sheetcount = count($sheetdata);
        if($sheetcount > 1){
            $user = array();
            $data = array();
            for ($i=1; $i<$sheetcount; $i++){
                // $id_user = $sheetdata[$i][1];
                $nim = $sheetdata[$i][2];
                $nik = $sheetdata[$i][3];              
                $nama = $sheetdata[$i][4];
                $tempat_lahir = $sheetdata[$i][5];
                $tgl_lahir = $sheetdata[$i][6];
                $agama = $sheetdata[$i][7];
                $no_hp = $sheetdata[$i][8];
                $email = $sheetdata[$i][9];
                $provinsi = $sheetdata[$i][10];
                $kab_kota = $sheetdata[$i][11];
                $kec = $sheetdata[$i][12];
                $alamat = $sheetdata[$i][13];
                $prodi = $sheetdata[$i][14];

                $user = array(
                    'username' => $sheetdata[$i][9],
                    'email' => $sheetdata[$i][9],
                    'password' => hash('sha256', $sheetdata[$i][3]),
                    'id_group' => 2
                );
                $this->user->set_user($user);
                $id_user = $this->db->insert_id();
                $data[] = array(
                    'id_user' => $id_user,
                    'nim' => $nim,
                    'nama' => $nama,
                    'nik'=> $nik,
                    'tempat_lahir' => $tempat_lahir,
                    'tgl_lahir' => $tgl_lahir,
                    'agama' => $agama,
                    'no_hp' => $no_hp,
                    'email' => $email,
                    'provinsi' => $provinsi,
                    'kab_kota' => $kab_kota,
                    'kec' => $kec,
                    'alamat' => $alamat,
                    'prodi' => $prodi,
                );
            }
            $importdata = $this->mhs->add_batch($data);
            if ($importdata){
                $this->session->set_flashdata('message_success', '<div class="alert alert-success">Import Data Berhasil!</div>');
                return redirect('Admin/v_mhs');
            }    
        }
    }

    public function importDosen(){
        $upload_file = $_FILES['file']['name'];
        $extension = pathinfo($upload_file, PATHINFO_EXTENSION);
        if($extension == 'csv'){
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
        }else if($extension =  'xlsx'){
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }else{
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        }
        $spreadsheet = $reader->load($_FILES['file']['tmp_name']);
        $sheetdata = $spreadsheet->getActiveSheet()->toArray();
        $sheetcount = count($sheetdata);
        if($sheetcount > 1){
            $user = array();
            $data = array();
            for ($i=1; $i<$sheetcount; $i++){
                // $id_user = $sheetdata[$i][1];
                $nama = $sheetdata[$i][2];
                $nip = $sheetdata[$i][3];              
                $gender = $sheetdata[$i][4];
                $prodi = $sheetdata[$i][5];
                $pendidikan = $sheetdata[$i][6];
                $id_group = $sheetdata[$i][7];
                $email = $sheetdata[$i][8];

                $user = array(
                    'username' => $sheetdata[$i][8],
                    'email' => $sheetdata[$i][8],
                    'password' => hash('sha256', $sheetdata[$i][3]),
                    'id_group' => 2
                );
                $this->user->set_user($user);
                $id_user = $this->db->insert_id();
                $data[] = array(
                    'id_user' => $id_user,
                    'nama' => $nama,
                    'nip' => $nip,
                    'gender'=> $gender,
                    'prodi' => $prodi,
                    'pendidikan' => $pendidikan,
                    'id_group' => $id_group,
                    'username' => $email,
                );
            }
            $importdata = $this->dosen->add_batch($data);
            if ($importdata){
                $this->session->set_flashdata('message_success', '<div class="alert alert-success">Import Data Berhasil!</div>');
                return redirect('Admin/v_dosen');
            }    
        }
    }

    public function tambahGroup(){
        $data = [
            'nama_group' => $this->input->post('nama_group'),
            'is_active' => $this->input->post('is_active') 
        ];

        $this->mhs->addGroup($data);
        echo json_encode(['success' =>true, 'message' => 'Group Berhasil Ditambahkan!']);
          
    }

    public function menuManagement(){        
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));
        $query = $this->db->get("user_group");
        $data = [];
        
        foreach($query->result() as $key => $r){
            $data[] = array(
                'no' => $key+1,
                'nama_group' => $r->nama_group,
                'action' =>'
                <button type="button"  data-toggle="modal" data-target="#form-akses" class="btn btn-warning">Akses</button>
                <button type="button"  data-toggle="modal" data-target="#form-edit" class="btn btn-primary">Edit</button>
                <button type="button"  data-toggle="modal" data-target="#form-hapus" class="btn btn-danger">Hapus</button>
                '
            ); 
        }
        $result = array(
            "draw" => $draw,
            "recordsTotal" => $query->num_rows(),
            "recordFiltered" => $query->num_rows(),
            "data" => $data
        );
        echo json_encode($result);
        exit();  
    }
   
    

}
