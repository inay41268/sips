<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->table = 'dosen';
        $this->load->model('M_Dosen', 'dosen');

    }

    public function index(){
        $data['username'] = $this->session->userdata('username');
        $this->load->view('templates/header');
		$this->load->view('templates/sidebar');
        $this->load->view('Dosen/accJudul');
		$this->load->view('templates/footer');  
    }

    public function get_items(){
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $query = $this->dosen->getAll();

        $data = [];

        foreach($query->result() as $key => $r){
            $data[] = array(
                'no' => $key+1,
                'id_skripsi' => $r->id_skripsi,
                'id_mhs' =>$r->id_mhs,
                'nim' =>$r->nim,
                'prodi' => $r->prodi,
                'angkatan' =>$r->angkatan,
                'dosen' =>$r->dosen,
                'judul' => $r->judul,
                'status_skripsi' => ($r->status_skripsi == "1" ? '<span class="badge badge-success">Accepted</span>' : '<a href="#" data-delete-url="' . site_url("dosen/acc/" . $r->id_mhs) . '" role="button" onclick="return verifConfirm(this)" class="btn btn-info hapus">Accepted Skripsi</a>')
            ); 
        }
        $result = array(
            "draw" => $draw,
            "recordsTotal" => $query->num_rows(),
            "recordFiltered" => $query->num_rows(),
            "data" => $data
        );
        echo json_encode($result);
        exit();
    }

    public function acc($id){
        $res = $this->dosen->accSkripsi($id, ['status_skripsi' => 1]);
        if ($res)
            echo json_encode(['success' => true, 'message' => 'Skripsi sudah acc!']);
        else echo "gj";
    }

}

?>