<?php

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('M_Auth', 'auth');
        $this->load->model('M_Mhs', 'mhs');
        $this->load->model('M_User', 'user');
    }  

    public function checkAlphaOnly($nama){
        if(!preg_match('/^[a-zA-Z ]*$/', $nama)) return FALSE ;
        else return TRUE;
    }

    public function checkNumber($no){
        if(!preg_match('/^(\+62|62|0)8[1-9][0-9]{6,9}$/', $no)) return FALSE;
        else return TRUE;
    }

    public function checkAge($age){
        $date1 = new DateTime("now");
        $date2 = new DateTime($age);
        $interval = $date1->diff($date2);
        if ($interval->y<16) return FALSE;
        else return TRUE;
    }

    public function register(){

        $data_agama = $this->auth->agama();
        $data['agama'] = $data_agama;
        $getdata = $this->auth->provinsi();
        $data['dataprov'] = $getdata;
        $data_prodi = $this->auth->prodi();
        $data['prodi'] = $data_prodi;

        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
                $this->load->view('register', $data);
        }else{
            $this->form_validation->set_rules('nim', 'Nim', 'required|max_length[10]|is_unique[mhs.nim]');
            $this->form_validation->set_rules('nama', 'Nama', 'required|callback_checkAlphaOnly');
            $this->form_validation->set_rules('nik', 'Nik', 'required|max_length[16]|numeric|is_unique[mhs.nim]');
            $this->form_validation->set_rules('t_lahir', 'Tempat lahir','required');
            $this->form_validation->set_rules('tgl_lahir', 'Tanggal lahir','required|callback_checkAge');
            $this->form_validation->set_rules('agama','Agama', 'required|integer');
            $this->form_validation->set_rules('hp', 'No Hp', 'required|callback_checkNumber');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_emails|is_unique[mhs.nim]');
            $this->form_validation->set_rules('prov_id', 'Prov_id', 'required|integer');
            $this->form_validation->set_rules('kab_id', 'Kab_id', 'required|integer');
            $this->form_validation->set_rules('kecamatan', 'Kec_id', 'required|integer');
            $this->form_validation->set_rules('prodi', 'Prodi', 'required|integer');
            $this->form_validation->set_rules('alamat', 'Alamat', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]|matches[confirm-password]');
            $this->form_validation->set_rules('confirm-password', 'Password', 'required|min_length[8]');

            $this->form_validation->set_message('required', '{field} harus diisi!');
            $this->form_validation->set_message('is_unique', '{field} sudah digunakan!');
            $this->form_validation->set_message('alpha_numeric_spaces', '{field} hanya berisi huruf dan spasi!');
            $this->form_validation->set_message('max_length', '{field} tidak lebih dari 16 karakter!');
            $this->form_validation->set_message('min_length', 'Masukkan minimal 8 karakter!');
            $this->form_validation->set_message('is_unique', '{field} sudah digunakan!');
            $this->form_validation->set_message('matches', '{field} tidak match!');
            $this->form_validation->set_message('numeric', '{field} harus hanya berisi huruf!');
            $this->form_validation->set_message('valid_emails', 'Masukkan {field} yang valid!');
            $this->form_validation->set_message('checkNumber', 'Gunakan format +628');
            $this->form_validation->set_message('checkAge', 'Tanggal lahir tidak valid');
            $this->form_validation->set_message('checkAlphaOnly', '{field} hanya berisi huruf dan spasi');
            $this->form_validation->set_message('match', '{field} tidak match!');


            if($this->form_validation->run()=== FALSE){
                $this->load->view('register', $data);
            }else{
                $user = array(
                    'username' => $this->input->post('email'),
                    'email' => $this->input->post('email'),
                    'password' => hash('sha256', $this->input->post('password')),
                    'id_group' => 2
                );

                $this->user->set_user($user);
                $id_user = $this->db->insert_id();
                $data = [
                            'id_user' => $id_user,
                            'nim' => $this->input->post('nim'),
                            'nama' => $this->input->post('nama'),
                            'nik'=> $this->input->post('nik'),
                            'tempat_lahir' => $this->input->post('t_lahir'),
                            'tgl_lahir' => $this->input->post('tgl_lahir'),
                            'agama' => $this->input->post('agama'),
                            'no_hp' => $this->input->post('hp'),
                            'email' => $this->input->post('email'),
                            'provinsi' => $this->input->post('prov_id'),
                            'kab_kota' => $this->input->post('kab_id'),
                            'kec' => $this->input->post('kecamatan'),
                            'alamat' => $this->input->post('alamat'),
                            'prodi' => $this->input->post('prodi'),
                            'password' => hash('sha256', $this->input->post('password'))
                        ]; 
                $daftar = $this->mhs->simpan($data);
                if ($daftar){
                    $this->session->set_flashdata('message_success', 'Pendaftaran Akun Berhasil!');
                    return redirect('auth/login');
            }
        }     
     }
    }

    public function login()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
			$this->load->view('login');
		} else {
			$username = $this->input->post('email');
			$password = $this->input->post('password');

            // $user = $this->db->get_where('user', ['email' => $username])->row();
            $user = $this->auth->userMhs($username)->row();
            // var_dump($user);
            // die;
            // Cek user sudah terdaftar
            if ($user){
                // Cek password sudah benar
                if (!password_verify($password, $user->password)) {
                    $this->session->set_flashdata('message_login_error', '<div class="alert alert-danger" role="alert">Password salah!</div>');
				    $this->load->view('login');
                }else{
                    $data = [
                        'email' => $user->email,
                        'id_group' => $user->id_group,
                        'id_user' => $user->id_user
                    ];
                    $this->session->set_userdata($data);
                    if($user->id_group=='1'){
                        redirect('admin/index');
                    }elseif($user->id_group=='2'){
                        redirect('mahasiswa/index');
                    }elseif($user->id_group=='3'){
                        redirect('dosen/index');
                    }
                }    
            }else{
                $this->session->set_flashdata('message_login_error', '<div class="alert alert-danger" role="alert">Email Belum Terdaftar!</div>');
				$this->load->view('login');
            }
		}
    }    

    public function logout(){
        $this->session->sess_destroy();
        redirect('auth/login', 'refresh'); 
    }
    
    public function getdatakab(){
        $prov_id = $this->input->post('provinsi');
        $kabupaten = $this->auth->kabupaten($prov_id);
        echo json_encode($kabupaten);
    }
    public function getdatakec(){
        $kab_id = $this->input->post('kabupaten');
        $kecamatan = $this->auth->kecamatan($kab_id);
        echo json_encode($kecamatan);
    }  
    
    public function forgot_password(){

        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        if($this->form_validation->run()==false){
            $this->load->view('forgot-password');
        }else{
            $email = $this->input->post('email');
            $result = $this->auth->change_password($email);

            if($result == NULL){
                $this->session->set_flashdata('message_error', 'Email belum terdaftar!');
                return redirect('auth/forgot_password');
            }else{
                $data['email'] = $result;
                $this->session->set_flashdata('message_success', '<div class="alert alert-danger" role="alert">Email Terdaftar!</div>');
                $this->load->view('change-password', $data);
            }
           
        }     
    }

    public function changePassword(){  
        $this->form_validation->set_rules('password1', 'Password1', 'trim|required|matches[password2]');
        $this->form_validation->set_rules('password2', 'Password2', 'trim|required|matches[password1]');
        if($this->form_validation->run()==false){
            $this->load->view('change-password');
        }else{
            $password =  password_hash($this->input->post('password1'),PASSWORD_DEFAULT)
            ;
            $email = $this->input->post('email');

            $result = $this->auth->update_password($email, $password);
			if (!$result) {
				$this->session->set_flashdata('message_error', 'Password gagal diupdate!');
				return redirect('/auth/forgot_password');
			} else {
				$this->session->set_flashdata('message_success', 'Password Berhasil diupdate!');
				return redirect('/auth/login');
			}
        }
        
    }

}